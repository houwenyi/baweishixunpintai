export default [
  {
    exact: true,
    path: '/index',
    component: '@/pages/Index',
    icon: 'dashboard',
    name: '工作台',
  },
  {
    path: '/article',
    icon: 'form',
    name: '文章',
    routes: [
      {
        path: '/article/all',
        icon: 'form',
        name: '所有文章',
        component: '@/pages/Article/All',
      },
      {
        path: '/article/classify',
        icon: 'copy',
        name: '分类管理',

        component: '@/pages/Article/Classify',
      },
      {
        path: '/article/label',
        icon: 'tag',
        name: '标签管理',

        component: '@/pages/Article/Label',
      },
    ],
  },
  {
    exact: true,
    path: '/login',
    component: '@/pages/Login',
    name: '登录',
    // 新页面打开
    target: '_blank',
    // 不展示顶栏
    headerRender: false,
    // 不展示页脚
    footerRender: false,
    // 不展示菜单
    menuRender: false,
    // 不展示菜单顶栏
    menuHeaderRender: false,
    // 权限配置，需要与 plugin-access 插件配合使用
    access: 'canRead',
    // 隐藏子菜单
    hideChildrenInMenu: true,
    // 隐藏自己和子菜单
    hideInMenu: true,
    // 在面包屑中隐藏
    hideInBreadcrumb: true,
    // 子项往上提，仍旧展示,
    flatMenu: true,
  },
  {
    path: '/page',
    component: '@/pages/Page',
    icon: 'copy',
    name: '页面管理',
  },
  {
    path: '/knowledge',
    component: '@/pages/Knowledge',
    icon: 'book',
    name: '知识小册',
  },
  {
    path: '/poster',
    component: '@/pages/Poster',
    icon: 'star',
    name: '海报管理',
  },
  {
    path: '/comment',
    component: '@/pages/Comment',
    icon: 'message',
    name: '评论管理',
  },
  {
    path: '/mailbox',
    component: '@/pages/Mailbox',
    icon: 'mail',
    name: '邮箱管理',
  },
  {
    path: '/file',
    component: '@/pages/File',
    icon: 'folder-open',
    name: '文件管理',
  },
  {
    path: '/search',
    component: '@/pages/Search',
    icon: 'search',
    name: '搜索记录',
  },
  {
    path: '/visit',
    component: '@/pages/Visit',
    icon: 'project',
    name: '访问统计',
  },
  {
    path: '/user',
    component: '@/pages/User',
    icon: 'user',
    name: '用户管理',
  },
  {
    path: '/system',
    component: '@/pages/System',
    icon: 'setting',
    name: '系统设置',
  },
];
