import { defineConfig } from 'umi';
import routes from './routes';
// 配置
export default defineConfig({
  routes: routes,
  //   热更新
  mfsu: {},
  //   可以通过配置文件配置 layout 的主题等配置
  layout: {
    name: '管理后台',
    logo: 'https://bwcreation.oss-cn-beijing.aliyuncs.com/2021-07-26/logo2.4a842ec4.png',
  },
  //开启antd
  antd: {
    dark: true,
  },
});
