// 在构建时是无法使用 dom 的，
// 所以有些配置可能需要运行时来配置，
// 我们可以在 [src/app.tsx](export const layout = ({) 中做如下配置:
import { RequestConfig } from 'umi';
export const layout = () => {

    return {
        rightContentRender: () => <div>头</div>,
        footerRender: () => <div>地下</div>,
        onPageChange: () => {
            // const { currentUser } = initialState;
            // const { location } = history;
            // // 如果没有登录，重定向到 login
            // if (!currentUser && location.pathname !== '/user/login') {
            //     history.push('/user/login');
            // }
        },
        // menuHeaderRender: undefined,
        // ...initialState?.settings,
    };
};

// 运行时配置
// 在 src / app.ts 中你可以配置一些运行时的配置项来实现部分自定义需求。示例配置如下：api
// 開啓接口
export const request: RequestConfig = {
    timeout: 1000,
    errorConfig: {},
    middlewares: [],
    requestInterceptors: [],
    responseInterceptors: [],
};