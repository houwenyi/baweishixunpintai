import { request } from "umi"
export const Login = (params: any) => {
    // promies 接口 axiso
    const res = request('https://creationapi.shbwyz.com/api/auth/login', {
        method: "post",
        data: {
            name: params.username,
            password: params.password
        },
        requestType: "form",
        skipErrorHandler: true,
    }).then((response) => {
        return response

    }).catch((error) => {
        return error
    })
    return res
}
